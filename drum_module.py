import mido
from pyo import *
import yaml
from os import listdir
from os.path import isfile, join
import re
import os
from config import Config

class Drums():
  def __init__(self):
    self.config = Config()

  def load_kit(self, kit_folder='acoustic_kit'):
    mypath = self.config.get_sounds_folder() + kit_folder
    file_names = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    return file_names

  def load_sounds(self):
    '''
    Loads sounds from "default_sounds_path/default_kit" sounds have to be in format
    ..._DisplayName-[0-100]
    After that creates dictionary: {"DisplayName":[file1, file2, file3,...]}
    '''
    file_names = self.load_kit()
    keys = {} #Refactor from here after V1.1
    values = []
    for file_name in sorted(file_names):
      key = re.search('(?!.*_)\w+(?=-\d+.wav)', file_name).group()
      if key not in keys:
        values = []
      value = self._load_sound_to_ram(file_name)
      dicter = {"note":self.config.get_note_from_drumpad(key), "velocity_min":0, "velocity_max":100, "sound":value}
      values.append(dicter)
      keys.update([(key, values)])
      #value = re.search('(?!-)\d+.wav', file_name).group()
    #to here
    return keys

  def start_drum_session(self, dictionary):
    for key in dictionary:
      number_of_sound_samples = len(dictionary[key])
      velocity_to_sound_ratio = 100 / number_of_sound_samples

      #Create session dictionary
      for i in range(number_of_sound_samples):
        velocity_min = i*velocity_to_sound_ratio
        velocity_max = (i+1)*velocity_to_sound_ratio
        dictionary[key][i]['velocity_min'] = velocity_min
        dictionary[key][i]['velocity_max'] = velocity_max

    return dictionary

  def _load_sound_to_ram(self, sound_path):
    table = SndTable("{}/{}".format(self.config.get_start_kit(), sound_path))
    freq = table.getRate()
    sound = TableRead(table=table, freq=freq)
    return sound

  def start_server(duplex=0, buffersize=256):
    s = Server(duplex, buffersize).boot()
    s.start()

def output_sound(sounds, message):
  drumpad = drums.config.get_drumpad_from_note(message.note)
  for i in range(len(sounds[drumpad])):
    if message.velocity >= sounds[drumpad][i]['velocity_min'] and message.velocity <= sounds[drumpad][i]['velocity_max']:
      print(sounds[drumpad][i])
      sounds[drumpad][i]['sound'].out() 


if __name__ == "__main__":
  from soundServer import SoundServer
  if os.name == 'nt':
    s = Server(duplex=0, buffersize=128, midi='jack')
  else:
    s = Server(duplex=0, buffersize=128)
  s.boot()
  s.start()
  drums = Drums()
  sounds = drums.load_sounds()
  sounds = drums.start_drum_session(sounds)

  print("midi inputs: {}".format(mido.get_input_names()))
  midi_in_port = input("Select MIDI Input (id): ")
  selected_port = mido.get_input_names()[int(midi_in_port)]

  #drum kit config --> Refactor to enum
  drums_config = drums.config.get_drum_kit_config()

  import time
  from pynput import keyboard
  with mido.open_input(selected_port) as inport:
    for message in inport:
      if message.type not in ['clock']:
        print(message.note)
        output_sound(sounds, message)
