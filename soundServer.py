import os
from pyo import *

class SoundServer():
    def __init__(self):
        pass

    def _start_server(self):
        if os.name == 'nt':
            s = Server(duplex=0, buffersize=128, midi='jack')
        else:
            s = Server(duplex=0, buffersize=128)
        s.boot()
        s.start()

    def output_sound(self, sounds, message):
        drumpad = drums.config.get_drumpad_from_note(message.note)
        for i in range(len(sounds[drumpad])):
            if message.velocity >= sounds[drumpad][i]['velocity_min'] and message.velocity <= sounds[drumpad][i]['velocity_max']:
                print(sounds[drumpad][i])
                sounds[drumpad][i]['sound'].out() 