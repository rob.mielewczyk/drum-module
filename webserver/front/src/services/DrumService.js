import http from "../http-common";

class DrumService {
    startPyoServer(){
        return http.get('/start/pyo')
    }

    stopPyoServer(){
        return http.get('/stop/pyo')
    }

    testConnection(){
        return http.get('test')
    }
}

export default new DrumService();