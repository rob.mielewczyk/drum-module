import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/drums",
      name: "drums",
      component: () => import("./views/DrumList")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./views/AddDrumKit")
    }
  ]
});