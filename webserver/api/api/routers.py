from rest_framework import routers
from drums.viewsets import DrumKitSet

router = routers.DefaultRouter()
router.register(r'drums', DrumKitSet)