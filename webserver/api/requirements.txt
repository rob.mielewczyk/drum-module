python-rtmidi==1.4.1
mido==1.2.9
pyo==1.0.1
PyYAML==5.3.1
django==3.0.8
djangorestframework==3.11.0
django-cors-headers==3.4.0
pydub==0.24.1