from django.db import models

class DrumPad(models.Model):
    name = models.CharField(max_length=25)
    noteNumber = models.IntegerField()

    def __str__(self):
        return f"{self.name}"

class DrumKit(models.Model):
    name = models.CharField(max_length=25)
    saved = models.BooleanField(default = False)

    @classmethod
    def select():
        print("selecting")

    def __str__(self):
        return f"{self.name}"

# Create your models here.
class SoundSet(models.Model):
    name = models.CharField(max_length=25)
    kit = models.ForeignKey(DrumKit, on_delete=models.PROTECT)
    drumpad = models.ForeignKey(DrumPad, on_delete = models.PROTECT)

    def __str__(self):
        return f"{self.name}"


class Sound(models.Model):
    soundSet = models.ForeignKey(SoundSet, on_delete=models.CASCADE)
    path = models.FileField(upload_to='sounds/')

    def __str__(self):
        return f"{self.path}"
