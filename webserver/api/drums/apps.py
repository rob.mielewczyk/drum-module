from django.apps import AppConfig


class DrumsConfig(AppConfig):
    name = 'drums'
