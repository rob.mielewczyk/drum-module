from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from pyo import Server
from pyo import *
import time
from api.helpers import ThreadWithExceptions
import os
import threading
from .models import Sound, DrumPad, SoundSet, DrumKit

queue = []

class TestConnection(APIView):
    def get(self, request):
        return Response({"message": "Server works"})

class Reload(APIView):
    def get(self, request):
        queue.append('reload')
        return Response({"message":"Reloading..."})

class Kits(APIView):
    def get(self, request):
        query = DrumKit.objects.all()
        return Response({"message": "Kits loaded", "kits": query})

    def post(self, request):
        if request.data.get('kit_name') is not None:
            kit_name = request.data['kit_name']
            select_kit = DrumKit.objects.get(kit_name)
            selected_kit.select()
            return Response({"message": f"succesfully loaded: {kit_name}"})
        else:
            return Response({"message": "You have to specify kit_name"}) 

class StartServer(APIView):
    
    def get(self, request):
        try:
            response = self._start_pyo_based_on_os()
            if isinstance(response, Response): return response
            return Response({"error": "error in StartServer /GET"})
        except:
            return Response({"error": "start pyo server error / are you sure you connected MIDI IN and"
                                      " you have an output device?"}, status=status.HTTP_409_CONFLICT)

    def _start_pyo_based_on_os(self):
        # Windows
        if os.name == 'nt':
            s = Server(duplex=0, buffersize=840, midi='jack')
        else:
            s = Server(duplex=0, buffersize=840)

        s.setMidiInputDevice(99) # Opens all midi devices
        s.boot().start()


        # Initialize server loop
        threading.Thread(target=self._drum_module_loop,
                         name='drum-loop', args=[s], daemon=True).start()

        # Return response that drum loop is active
        return Response({"data": 'Drum loop thread started'})

    def _event(self, status, note, velocity):
        '''
        This function outputs midi sound
        '''
        print(note, velocity)
        for output in self.output[note]:
            if velocity>=output['min'] and velocity<=output['max']:
                output['sound'].out()

    #extract later
    def _load_sound_to_ram(self, sound_path):
        table = SndTable(f"{sound_path}")
        freq = table.getRate()
        sound = TableRead(table=table, freq=freq)
        return sound

    def _drum_module_loop(self, pyo_server, *args, **kwargs):
        pyo_server.setMidiInputDevice(99) # Opens all midi devices
        pyo_server.boot().start()

        midi = RawMidi(self._event)
        while True:
            if 'reload' in queue:
                queue.pop()
                self.reload()

    def reload(self):
        print("reloading")
        self.output = {}

        # Get all sounds to their notes
        self.get_all_sounds_to_their_notes()

    def get_all_sounds_to_their_notes(self):
        selected_kit = self.get_currently_selected_kit()
        soundsets = self.get_all_sounds_for_drumkit(selected_kit)
        for soundset in soundsets:
            note = soundset.drumpad.noteNumber
            sounds = []
            for sound in self.get_all_paths_for_sounds(soundset):
                sounds.append(self._load_sound_to_ram(sound))
            sounds = self._build_sound_meta(sounds)
            print(sounds)
            self.output[note] = sounds
        
    # Refactor to separate file
    def _build_sound_meta(self, sounds):
        sounds_with_meta = []
        step = int(128/len(sounds))
        for i, sound in enumerate(sounds):
            min_velocity = i*step
            max_velocity = (i+1)*step
            sounds_with_meta.append({
                "min": min_velocity,
                "max": max_velocity,
                "sound": sound
            })
        return sounds_with_meta


    #Refactor to separate file
    def get_currently_selected_kit(self):
        return DrumKit.objects.get(saved=True)

    def get_all_sounds_for_drumkit(self, drumkit):
        return SoundSet.objects.filter(kit = drumkit)

    def get_all_paths_for_sounds(self, soundset):
        return Sound.objects.filter(soundSet=soundset)



