from django.contrib import admin
from .models import DrumPad
from .models import SoundSet
from .models import Sound
from .models import DrumKit

# Register your models here.
@admin.register(DrumPad)
class DrumPadAdmin(admin.ModelAdmin):
    list_display = ('name', 'noteNumber')

@admin.register(SoundSet)
class SoundSetAdmin(admin.ModelAdmin):
    list_display = ('name', 'kit', 'drumpad')

@admin.register(Sound)
class SoundAdmin(admin.ModelAdmin):
    list_display = ('soundSet', 'path')

@admin.register(DrumKit)
class SoundAdmin(admin.ModelAdmin):
    list_display = ['name']
