# About this project
Drum modules are used when you want to produce drum kit sounds after striking electronic drum (or sending any MIDI input).<br><br>
This project allows to play any sound defined by the user, mapped to any drum pad(MIDI in) that the user wants. Project can be run even on Rasberry PI without any unpleasant lag.

## Requirements
- Python(3.7.3) with installed requirements.txt
- Recommended: Good Sound Card -> Tested on (Steinberg UR22)

## How to run
1. Setup your sound card (Most systems just check default card to the best one in sound settings) alsamixer on RasberryPi
![](readme_images\sound_card_option.PNG)

2. > python -m pip install -r requirements.txt
3. > ./additional_requirements # on linux(debian)
4. > python manage.py runserver 
5. > vue.js frontend does have a main page but it's not yet fully implemented (work in progress)

## Why I created this project
Unfortunately sometimes things break and that's what happened to my drum module that came with my electronic drums. Unfortunately after trying many alternatives (FL Studio, Various VST plugins) I discovered that all of them were either very limited, crashed, or were a pain to setup correctly.
<br><br>
That's why I created this easy to use Drum-module that should work out of the box if all requirements are met. 

---
## Public Resources used in this project
*Karen Arnold - repository picture (woman playing drums)*