class KeyboardController():
    '''
    Used for sending MIDI messages via keyboard
    '''
    def __init__():
        self._start_key_listener()

    def _on_press(key):
        print('pressed')
        message = mido.Message('note_on', note=43, velocity=90)
        sounder.append(message)

    def _on_release(key):
         pass

    def _start_key_listener():
    listener = keyboard.Listener(
        on_press=self.on_press,
        on_release=self.on_release)
    listener.start()