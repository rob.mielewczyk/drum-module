import yaml

class Config():
  def __init__(self):
    self._reload_changes()
    self._init_drumpads_from_note()

  def get_sounds_folder(self):
    return self.config.get('startup').get('default_sounds_path')

  def get_start_kit(self):
    return self.config.get('startup').get('default_sounds_path') + self.config.get('startup').get('default_kit')

  def get_drum_kit_config(self):
    return self.config.get('drum_kit')

  
  def _preload_sounds_to_session(loaded_sounds_for_instrument):
    divider = len(loaded_sounds_for_instrument)
    expr = 100 / divider

    for i in range(divider):
      if message.velocity() > i*divider and message.velocity() <(i+1)*divider:
        loaded_sounds_for_instrument[i].out()

  def _init_drumpads_from_note(self):
    drumkit = self.get_drum_kit_config()
    self.reverse_dict = {v:k for k, v in drumkit.items()}

  def get_drumpad_from_note(self, note):
    return self.reverse_dict.get(note)        

  def get_note_from_drumpad(self, name):
    '''
    returns: note
    '''
    return self.get_drum_kit_config().get(name)


  def _reload_changes(self):
    with open("config.yaml", 'r') as yaml_config:
      self.config = yaml.load(yaml_config, Loader=yaml.FullLoader)

  def _save_changes(self, changes):
    with open('config.yaml', 'w') as f:
        yaml.dump(changes, f)